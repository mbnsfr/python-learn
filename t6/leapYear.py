# mobina saffary
# day of year

year = int(input("enter the year : "))
day = int(input("enter the day : "))
leap = False
month = ""
error = False

# leap year
if (year % 4 == 0):
    if (year % 100 == 0):
        if (year % 400 == 0):
            leap = True
    else:
        leap = True

# which day of month and which month
if(day <= 31):  # farvardin
    month = "farvardin"
elif (31 < day <= 62):  # ordibehesht
    day = day - 31
    month = "ordibehesht"
elif (62 < day <= 93):   # khordad
    day = day - 62
    month = "khordad"
elif (93 < day <= 124):  # tir
    day = day - 93
    month = "tir"
elif (124 < day <= 155):  # mordad
    day = day - 124
    month = "mordad"
elif (155 < day <= 186):  # shahrivar
    day = day - 155
    month = "shahrivar"
elif (186 < day <= 216):  # mehr
    day = day - 186
    month = "mehr"
elif (216 < day <= 246):  # aban
    day = day - 216
    month = "aban"
elif (246 < day <= 276):  # azar
    day = day - 246
    month = "azar"
elif (276 < day <= 306):  # dey
    day = day - 276
    month = "dey"
elif (306 < day <= 336):  # bahman
    day = day - 306
    month = "bahman"
elif (336 < day <= 365):  # esfand
    day = day - 336
    month = "esfand"
elif (leap and day == 366):
    error = True
else:
    error = True

if (error):
    print("enter correct values !")
else:
    a = "{} - {} - {}"
    print(a.format(day, month, year))
