from termcolor import colored
import emoji

firstName = []
lastName = []
phone = []
email = []
nationalCode = []
password = []
userName = []
cart = []
uid = {"1"}
uid.pop()
products = ['گوشت   ', 'بوقلمون', 'ماهی   ', 'میگو   ',
            'مرغ    ', 'شنیسل  ', ' سوسیس ', 'ژامبون ', 'بلدرچین']
price = ['12352', '23442', '14345', '25567',
         '72589', '89251', '82452', '46654', '54687']
productsId = [1, 2, 3, 4,
              5, 6, 7, 8, 9]
signIn = False
enteredId = None


def sign_in():

    global password
    global userName
    global firstName
    global lastName
    global signIn
    global enteredId
    global uid

    print("\033[H\033[J")

    if userName == []:
        print(colored("there is no user in database sign up and try again", "yellow"))

    else:
        if signIn == False:
            print("Please enter your info to get into app :")
            incomingUseruserName = input("user name : ")
            incomingUserpassword = input("password : ")
            for i in uid:
                if (userName[i] == incomingUseruserName):
                    if (password[i] == incomingUserpassword):
                        signIn = True
                        enteredId = i

                    else:
                        print(colored("your information is wrong", "red"))

                else:
                    print(colored("your information is wrong", "red"))

            if (signIn == True):
                print("welcome " + firstName[enteredId] +
                      " " + lastName[enteredId])

        else:
            print(colored("your logged in before", "yellow"))

    print(colored("__________________________________________________________________________________ ", "grey"))
    Start()


def sign_up():

    global firstName
    global lastName
    global phone
    global email
    global nationalCode
    global password
    global userName
    global uid
    global signIn
    global enteredId
    global cart

    print("\033[H\033[J")
    print("Please enter your information : ")
    First_Name = input("FirstName : ")
    Last_Name = input("LastName : ")
    IPhone = input("Phone : ")
    IEmail = input("Email : ")
    INationalCode = input("NationalCode : ")
    IPassword = input("Password : ")
    IUserName = input("UserName : ")

    Id = len(uid)
    firstName.append(First_Name)
    lastName.append(Last_Name)
    phone.append(IPhone)
    email.append(IEmail)
    nationalCode.append(INationalCode)
    password.append(IPassword)
    userName.append(IUserName)
    cart.append([])
    uid.add(Id)

    signIn = True
    enteredId = Id

    text = First_Name + " " + Last_Name
    print("welcome " + colored(text, "green",
                               attrs=['bold', 'underline']) + "\nyour logged in")

    print(colored("__________________________________________________________________________________ ", "grey"))
    Start()


def show_profile():

    global firstName
    global lastName
    global signIn
    global phone
    global email
    global nationalCode
    global enteredId

    print("\033[H\033[J")

    if signIn == True:
        text = "firstName    :   " + firstName[enteredId] + " \n" + "lastName     :   " + lastName[enteredId] + " \n" + "phone        :   " + \
            phone[enteredId] + " \n" + "email        :   " + email[enteredId] + \
            " \n" + "nationalCode :   " + nationalCode[enteredId] + " \n"
        print(colored("your information is\n", "green"))
        print(colored(text, "blue"))

    else:
        print(colored("please log in first", "yellow"))

    print(colored("__________________________________________________________________________________ ", "grey"))
    Start()


def show_cart():

    global signIn
    global cart
    global price
    global products
    global enteredId

    print("\033[H\033[J")
    print("your cart contains :")

    if signIn == True:
        if cart[int(enteredId)] != []:
            for i in cart[int(enteredId)]:
                print(products[i-1] + " : " + price[i-1])
    else:
        print(colored("please log in first", "yellow"))

    print(colored("__________________________________________________________________________________ ", "grey"))
    Start()


def shopping():

    global enteredId
    global cart
    global products
    global price
    global productsId

    print("\033[H\033[J")

    if signIn == True:
        print("id  |   products    |    price    |    stock   ")
        print("_______________________________________________ ")
        for i in productsId:
            print(" "+str(i)+"  |   "+str(products[i-1]) + "     |    " +
                  str(price[i-1]) + "    |    yes")
        client = input(
            "\nenter the id's of products you want : (like this : 1,5,8)")
        cart[int(enteredId)] = client.split(",")
        print(colored("your cart updated !", "green"))

    else:
        print(colored("please log in first", "yellow"))

    print(colored("__________________________________________________________________________________ ", "grey"))
    Start()


def order_register():

    global enteredId
    global cart
    global products
    global price
    global productsId

    print("\033[H\033[J")
    print("your cart contains :\n")

    if signIn == True:
        full_price = 0
        print(" id  |   products    |    price    ")
        print("__________________________________ ")
        for i in cart[int(enteredId)]:
            print(" "+str(i)+"   |   "+products[i-1] + "     |    " +
                  price[i-1] + "    ")
            full_price = full_price + int(price[i-1])
        print("\nfull price : ", str(full_price))
        client = input(
            "\nconfirm your list : ( "+colored("no -> n", "red")+" / "+colored("yes -> y", "green")+" )")
        if client == "y":
            print("you should pay " + str() + " Rial")
            ans = input("confirm your payment : ( "+colored("no -> n",
                                                            "red")+" / "+colored("yes -> y", "green")+" )")
            if ans == "y":
                print(colored(
                    "your list in registered and payed.you will receive it as soon as possible\nthanks for your shopping", "green"))
                cart[int(enteredId)] = []

            else:
                print(colored("payment proccess stopped", "red"))

        else:
            print(colored("confirm proccess stopped", "red"))

    else:
        print(colored("please log in first", "yellow"))

    print(colored("__________________________________________________________________________________ ", "grey"))
    Start()


def Start():

    print(colored("welcome to final python project designed by ", "green")+colored(
        "mobina saffary", "green", attrs=['bold', 'underline'])+colored(
            " and ", "green")+colored(
                "fateme kazemi", "green", attrs=['bold', 'underline']))
    print("this program designed for "+colored("butcher's shop ",
                                               "red")+emoji.emojize(':poultry_leg:'))
    print("if you wanna use this program choose one of our facilities")
    print(colored("sign in ", "blue")+": (press "+colored("A", "blue")+" key)")
    print(colored("sign up ", "blue")+": (press "+colored("B", "blue")+" key)")
    print(colored("my profile ", "blue") +
          ": (press "+colored("C", "blue")+" key)")
    print(colored("your cart ", "blue") +
          ": (press "+colored("D", "blue")+" key)")
    print(colored("shop & products ", "blue") +
          ": (press "+colored("E", "blue")+" key)")
    print(colored("order register ", "blue") +
          ": (press "+colored("F", "blue")+" key)")
    print(colored("exit : (press any key )", "red"))
    Input = input()
    if ((Input == "a") or (Input == "A")):
        sign_in()
    elif ((Input == "b") or (Input == "B")):
        sign_up()
    elif ((Input == "c") or (Input == "C")):
        show_profile()
    elif ((Input == "d") or (Input == "D")):
        show_cart()
    elif ((Input == "e") or (Input == "E")):
        shopping()
    elif ((Input == "f") or (Input == "F")):
        order_register()


Start()
