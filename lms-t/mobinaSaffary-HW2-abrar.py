a = {"a":1,"b":2}
b = {"a":"1","b":"2"}
print( a == b)
#False

a = {"a":"2","b":"1"}
b = {"a":"1","b":"2"}
print( a == b)
#False

a = {"a":"1","b":"2"}
b = {"a":"1","b":"2","c":"0"}
print( a == b)
#False

a = {"a":"1","b":"2"}
b = {"a":"1","b":"2"}
print( a == b)
#true
